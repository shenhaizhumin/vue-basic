import { createRouter, createWebHistory} from 'vue-router'
import HomeView from '@/views/MainContent/HomeView.vue'
import FirstTableView from '@/views/MainContent/tabContent/FirstTableView.vue'
import DescriptionView from '@/views/MainContent/tabContent/DescriptionView.vue'
import DeptTableView from '@/views/MainContent/tabContent/DeptTableView.vue'
import LoginView from '@/views/User/LoginView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      component: HomeView,
      children:[
        {
          path: 'descript',
          name: 'descript',
          component: DescriptionView,
        },
        {
          path: 'ftable',
          name: 'ftable',
          component: FirstTableView
        },
        {
          path: 'dept',
          name: 'dept',
          component: DeptTableView
        },
      ]
    },
    {
      path: '/ttable',
      name: 'ttable',
      component: FirstTableView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    }
  ]
})

// router.beforeEach((to, from) => {
//   if (
//     // 检查用户是否已登录
//     !sessionStorage.getItem('token') &&
//     // 避免无限重定向
//     to.name !== 'login'
//   ) {
//     return {name :'login'}
//   }
// })

export default router
