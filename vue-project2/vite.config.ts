import { fileURLToPath, URL } from 'node:url'
import AutoImport from 'unplugin-auto-import/vite'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    AutoImport({ /* options */ 
    	imports: ['vue']
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server:{
    host: '0.0.0.0',
    proxy: { 
      '/api': {
       target: "http://120.25.124.223:8000",
       changeOrigin: true,
       rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  }
})
